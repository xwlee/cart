Cart
====

A shopping cart application.

# Setup

1. Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) and [Vagrant](http://www.vagrantup.com/downloads.html).
2. Clone the repo and cd into Cart directory.
3. Run `composer install`.
4. Run `vagrant up`.
5. Navigate to http://192.168.33.21 in browser. Or add to `192.168.33.21 cart.dev` hosts file and navigate to http://cart.dev in browser.
