<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Cart</title>

        {{ HTML::style('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css') }}
        <style>
            body { padding-top: 70px; }
            a.text-muted:hover { color: #c5c5c5; }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{ HTML::link('/', 'Cart', ['class' => 'navbar-brand']) }}
                </div>
                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ route('products.index') }}">Products</a></li>
                        <li><a href="{{ route('categories.index') }}">Categories</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::check())
                            <li><a href="{{ url('logout') }}">Logout</a></li>
                        @else
                            <li><a href="{{ url('login') }}">Login</a></li>
                        @endif
                    </ul>
                </div> 
            </div>
        </nav>

        <div class="container">
            @if (Session::has('message'))
                <div class="alert alert-{{ Session::get('message_type') }} alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">Close</span>
                    </button>
                    {{ Session::get('message') }}
                </div>
            @endif

            @yield('content')
        </div>

        {{ HTML::script('//cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js') }}
        {{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js') }}
    </body>
</html>
