<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
        <div>
            Hi {{ $username }},
        </div><br/>

        <div>
            Your new user account with the email address {{ $email }} is now set up.
        </div><br/>

        <div>
            Please use the link below to active your account.
            <a href="{{ $activationUrl }}" target="_blank">Click here</a>
        </div>
	</body>
</html>
