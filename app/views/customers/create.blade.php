@extends('layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <strong>Please register</strong>
            </h3>
        </div>
        <div class="panel-body">
            {{ Form::open(['route' => 'customers.store', 'role' => 'form']) }}                     

                <div class="form-group @if ($errors->has('username')) has-error has-feedback @endif">
                    {{ Form::label('username', 'Username') }}
                    {{ Form::text('username', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                    {{ $errors->first('username', '<p class="help-block text-danger">:message</p>') }}
                </div>

                <div class="form-group @if ($errors->has('email')) has-error has-feedback @endif">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::text('email', null, ['class' => 'form-control']) }}
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                    {{ $errors->first('email', '<p class="help-block text-danger">:message</p>') }}
                </div>

                <div class="form-group @if ($errors->has('password')) has-error has-feedback @endif">
                    {{ Form::label('password', 'Password') }}
                    {{ Form::password('password', ['class' => 'form-control']) }}
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                    {{ $errors->first('password', '<p class="help-block text-danger">:message</p>') }}
                </div>

                <div class="form-group @if ($errors->has('password_confirmation')) has-error has-feedback @endif">
                    {{ Form::label('password_confirmation', 'Confirm password') }}
                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                    {{ $errors->first('password_confirmation', '<p class="help-block text-danger">:message</p>') }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Register', ['class' => 'btn btn-success']) }}
                </div>
            
            {{ Form::close() }}
        </div>
    </div>
@stop
