@extends('layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <strong>Please login</strong>
            </h3>
        </div>
        <div class="panel-body">
            {{ Form::open(['route' => 'sessions.store', 'role' => 'form']) }}

                <div class="form-group @if ($errors->has('email')) has-error has-feedback @endif">
                    {{ Form::label('email', 'Email', ['class' => 'control-label']) }}
                    {{ Form::text('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }} 
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                    {{ $errors->first('email', '<p class="help-block text-danger">:message</p>') }}
                </div>

                <div class="form-group @if ($errors->has('email')) has-error has-feedback @endif">
                    {{ Form::label('password', 'Password', ['class' => 'control-label']) }}
                    {{ Form::password('password', ['class' => 'form-control']) }} 
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                    {{ $errors->first('password', '<p class="help-block text-danger">:message</p>') }}
                </div>

                <div class="checkbox">
                    <label>{{ Form::checkbox('remember') }} <strong>Remember Me</strong></label>
                </div>

                <div class="form-group">
                    {{ Form::submit('Sign in', ['class' => 'btn btn-success']) }}
                </div>

                <ul class="list-inline">
                    <li><a href="{{ url('register') }}" class="text-muted"><u>Create an account</u></a></li>
                    <li><a href="{{ route('password.remind') }}" class="text-muted"><u>Forgot Your Password?</u></a></li>
                <ul>

            {{ Form::close() }}
        </div>
    </div>
@stop
