@extends('layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <strong>Reset your password</strong>
            </h3>
        </div>
        <div class="panel-body">
            {{ Form::open(['route' => 'password.remind', 'role' => 'form']) }}

                <div class="form-group @if (Session::has('error')) has-error has-feedback @endif">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::text('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                    <span class="glyphicon glyphicon-remove form-control-feedback"></span>
                </div>

                <div class="form-group">
                    {{ Form::submit('Reset password', ['class' => 'btn btn-success']) }}
                </div>

            {{ Form::close() }}
        </div>
    </div>
@stop
