@extends('layout')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">
                <strong>Fill in the fields below to set your new password</strong>
            </h3>
        </div>
        <div class="panel-body">
            {{ Form::open(['route' => 'password.reset', 'role' => 'form']) }}
                {{ Form::hidden('token', $token) }}

                <div class="form-group @if (Session::has('error')) has-error has-feedback @endif">
                    {{ Form::label('email', 'Email') }}
                    {{ Form::text('email', null, ['class' => 'form-control', 'autofocus' => 'autofocus']) }}
                </div>

                <div class="form-group @if (Session::has('error')) has-error has-feedback @endif">
                    {{ Form::label('password', 'New password') }}
                    {{ Form::password('password', ['class' => 'form-control']) }}
                </div>

                <div class="form-group @if (Session::has('error')) has-error has-feedback @endif">
                    {{ Form::label('password_confirmation', 'Confirm new password') }}
                    {{ Form::password('password_confirmation', ['class' => 'form-control']) }}
                </div>

                <div class="form-group">
                    {{ Form::submit('Reset password', ['class' => 'btn btn-success']) }}
                </div>

            {{ Form::close() }}
        </div>
    </div>
@stop
