<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

# Authentication
Route::get('/register', 'CustomersController@create');
Route::resource('customers', 'CustomersController', [
    'only' => ['create', 'store']
]);
Route::get('customers/{id}/activate/{activationKey}', ['as' => 'customers.activate', 'uses' => 'CustomersController@activate']);
Route::get('/login', 'SessionsController@create');
Route::get('/logout', 'SessionsController@destroy');
Route::resource('sessions', 'SessionsController', [
    'only' => ['create', 'store', 'destroy']
]);

# Password Resets
 Route::get('password/remind', ['as' => 'password.remind', 'uses' => 'RemindersController@getRemind']);
 Route::post('password/remind',['as' => 'password.remind', 'uses' => 'RemindersController@postRemind']);
 Route::get('password/reset/{token}', ['as' => 'password.reset', 'uses' => 'RemindersController@getReset']);
 Route::post('password/reset', ['as' => 'password.reset', 'uses' => 'RemindersController@postReset']);

# Products
Route::get('/', ['as' => 'home', 'uses' => 'ProductsController@index']);
Route::resource('products', 'ProductsController');

# Categories
Route::resource('categories', 'CategoriesController');
