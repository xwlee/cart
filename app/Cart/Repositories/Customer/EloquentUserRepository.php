<?php namespace Cart\Repositories\Customer;

use Illuminate\Database\Eloquent\Model;
use Customer;

class EloquentCustomerRepository implements CustomerRepositoryInterface {

    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $customer;

    /**
     * Create a new EloquentUserRepository instance.
     *
     * @param \Illuminate\Database\Eloquent\Model
     * @return void
     */
    public function __construct(Model $customer) 
    {
        $this->customer = $customer;
    }

    /**
     * Create a customer.
     *
     * @param array $data
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(array $data) 
    {
        $this->customer->username = $data['username'];
        $this->customer->email    = $data['email'];
        $this->customer->password = $data['password'];

        $this->customer->save();

        return $this->customer;
    }

    /**
     * Find a user by it's id.
     *
     * @param integer $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id) 
    {
        return $this->customer->find($id);
    }

    /**
     * Update a customer.
     *
     * @param  \Illuminate\Database\Eloquent\Model $customer Customer Model
     * @param  array                               $data     Array of inputs
     * @return boolean
     */
    public function update(Customer $customer, array $input) 
    {
        $customer->fill($input);

        if ($customer->save()) {
            return true;
        }

        return false;
    }


}
