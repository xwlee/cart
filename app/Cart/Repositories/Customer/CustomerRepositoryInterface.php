<?php namespace Cart\Repositories\Customer;

interface CustomerRepositoryInterface {

    /**
     * Create a customer.
     *
     * @param array $data
     * @return Customer
     */
    public function create(array $data);

    /**
     * Find a customer by it's id.
     *
     * @param integer $id
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findById($id);

    /**
     * Update a customer.
     *
     * @param  Customer $customer Customer Model
     * @param  array    $data     Array of inputs
     * @return boolean
     */
    public function update(\Customer $customer, array $input);

}
