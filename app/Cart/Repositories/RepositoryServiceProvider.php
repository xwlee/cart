<?php namespace Cart\Repositories;

use Illuminate\Support\ServiceProvider;
use Cart\Repositories\Customer\EloquentCustomerRepository;
use Customer;

class RepositoryServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() 
    {
        $this->app->bind('Cart\Repositories\Customer\CustomerRepositoryInterface', function($app) 
        {
            return new EloquentCustomerRepository(new Customer); 
        });
    }

}
