<?php namespace Cart\Services\Forms;

class LoginForm extends FormValidator {

    /**
     * Validation rules for login
     *
     * @var array
     */
    protected $rules = [
        'email'    => 'required',
        'password' => 'required',
    ];

}
