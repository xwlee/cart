<?php namespace Cart\Services\Forms;

class RegistrationForm extends FormValidator {

    /**
     * Validation rules for registration
     *
     * @var array
     */
    protected $rules = [
        'username' => 'required|min:4|alpha_num',
        'email'    => 'required|email|min:5|unique:customers',
        'password' => 'required|min:6|confirmed',
    ];

}
