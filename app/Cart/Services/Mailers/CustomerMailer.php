<?php namespace Cart\Services\Mailers;

use Customer;

class CustomerMailer extends Mailer {

    /**
     * Send activation email to customer
     * 
     * @param \Customer
     * @return void
     */
    public function activate(Customer $customer) 
    {
        $view = 'customers.activate';
        $data = [
            'username'      => $customer->username,
            'email'         => $customer->email,
            'activationUrl' => route('customers.activate', array($customer->id, $customer->activation_key)) 
        ];
        $subject = 'Cart Account Activation';

        $this->sendTo($customer, $subject, $view, $data);
    }

}
