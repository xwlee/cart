<?php namespace Cart\Observers\Customer;

use Cart\Services\Mailers\CustomerMailer;

class CustomerObserver {

    protected $customerMailer;

    function __construct(CustomerMailer $customerMailer)
    {
        $this->customerMailer = $customerMailer;
    }

    public function creating($customer) 
    {
        // Generate activation key for the user being created
        $customer->activation_key = md5(uniqid(rand(), true));
    }

    public function created($customer)
    {
        $this->customerMailer->activate($customer);
    }

}
