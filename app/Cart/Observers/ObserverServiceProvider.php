<?php namespace Cart\Observers;

use Illuminate\Support\ServiceProvider;
use Customer;
use Cart\Observers\Customer\CustomerObserver;
use Cart\Services\Mailers\CustomerMailer;

class ObserverServiceProvider extends ServiceProvider {

    public function register() 
    {

    }

    public function boot() 
    {
        Customer::observe(new CustomerObserver(new CustomerMailer));
    }

}
