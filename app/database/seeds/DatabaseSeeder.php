<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

         $this->call('CustomersTableSeeder');
         $this->call('CategoriesTableSeeder');
         $this->call('ProductsTableSeeder');
         $this->call('OrdersTableSeeder');
         $this->call('OrderLinesTableSeeder');
	}

}
