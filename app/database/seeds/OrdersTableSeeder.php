<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrdersTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

        $customers = Customer::all();

        foreach ($customers as $customer) 
        { 
            foreach(range(1, 5) as $index)
            {
                Order::create([
                    'customer_id' => $customer->id,
                ]);
            }
        }
	}

}