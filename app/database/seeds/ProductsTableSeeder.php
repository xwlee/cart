<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class ProductsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

        $categories = Category::all();

        foreach ($categories as $category) 
        {
            for ($i = 0; $i < rand(-1, 10); $i++)
            {
                Product::create([
                    'name'            => ucwords($faker->word),
                    'manufacturer'    => ucwords($faker->word),
                    'unit_list_price' => $faker->randomFloat(2, 1, 100),
                    'category_id'     => $category->id,
                ]);
            }
        }
	}

}