<?php

// Composer: "fzaninotto/faker": "v1.3.0"
use Faker\Factory as Faker;

class OrderLinesTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();
        $products = Product::all()->toArray();

        $orders = Order::all();

        foreach ($orders as $order) 
        {
            $used = [];

            foreach(range(1, 5) as $index)
            {
                $product = $faker->randomElement($products);

                // We don't want same product appear in an order
                if ( ! in_array($product['id'], $used))
                {
                    OrderLines::create([
                        'order_id'        => $order->id,
                        'product_id'      => $product['id'],
                        'quantity'        => $faker->numberBetween(1, 10),
                        'unit_sale_price' => $product['unit_list_price'],
                    ]);
                }

                $used[] = $product['id'];
            }
        }
	}

}