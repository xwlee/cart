<?php

use Cart\Repositories\Customer\CustomerRepositoryInterface;
use Cart\Services\Forms\RegistrationForm;

class CustomersController extends \BaseController {

    protected $registrationForm;

    function __construct(CustomerRepositoryInterface $customer, RegistrationForm $registrationForm)
    {
        $this->customer = $customer;
        $this->registrationForm = $registrationForm;
    }

    /**
     * Show signup form
     * GET /register
     *
     * @return Response
     */
    public function create()
    {
        return View::make('customers.create');
    }

    /**
     * Process signup form
     * POST /register
     *
     * @return Response
     */
    public function store()
    {
        try 
        {
            $this->registrationForm->validate(Input::all());
    
            $this->customer->create(Input::all());

            return Redirect::to('/login')
                ->withMessage('Successfully registered. Please check your email and activate your account.')
                ->withMessageType('success');
        }
        catch (Cart\Services\Forms\FormValidationException $e)
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($e->getErrors());
        }
    }

    /**
     * Activate account
     * GET /customer/{id}/activate/{activationKey}
     *
     * @return Response
     */
    public function activate($id, $activationKey) 
    {
        $customer = $this->customer->findById($id);

        if ($customer->isActive()) 
        {
            return Redirect::to('/')
                ->withMessage('This account is already activated.')
                ->withMessageType('danger');
        }
            
        if ( ! $customer->isValidActivationKey($activationKey))
        {
            return Redirect::to('/')
                ->withMessage('Invalid activation key.')
                ->withMessageType('danger');
        }
        
        if ($this->customer->update($customer, ['active' => 1])) {
            Auth::login($customer);

            return Redirect::to('/')
                ->withMessage('Your account is now activated.')
                ->withMessageType('success');
        }
    }

}
