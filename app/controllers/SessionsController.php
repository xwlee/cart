<?php

use Cart\Services\Forms\LoginForm;

class SessionsController extends \BaseController {

    protected $loginForm;

    function __construct(LoginForm $loginForm)
    {
        $this->loginForm = $loginForm;
    }

	/**
	 * Show login form
     * GET /login
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('sessions.create');
	}


	/**
     * Process login form
     * POST /login
	 *
	 * @return Response
	 */
	public function store()
	{
        try 
        {
            $credentials = Input::only(['email', 'password']);
            $remember    = Input::get('remember', false);

            $this->loginForm->validate($credentials);
        }
        catch (Cart\Services\Forms\FormValidationException $e)
        {
            return Redirect::back()
                ->withInput()
                ->withErrors($e->getErrors());
        }

        if (Auth::attempt($credentials, $remember)) 
        {
            return Redirect::intended('/');
        }
        else
        {
            return Redirect::to('login')
                ->withMessage('Could not verify your credentials.')
                ->withMessageType('warning');
        }
	}

	/**
	 * Process logout
     * GET /logout
	 *
	 * @return Response
	 */
	public function destroy()
	{
        Auth::logout();

        return Redirect::to('/login');
	}


}
