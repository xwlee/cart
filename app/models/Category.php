<?php

class Category extends \Eloquent {

	protected $guarded = ['id'];

    protected $softDelete = true;

    public function products()
    {
        return $this->hasMany('Product');
    }

}