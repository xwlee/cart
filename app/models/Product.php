<?php

class Product extends \Eloquent {

    protected $guarded = ['id'];

    protected $softDelete = true;

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function orders()
    {
        return $this->belongsToMany('Order', 'order_lines');
    }

}