<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Customer extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'customers';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    protected $guarded = ['id'];

    protected $softDelete = true;

    public function orders()
    {
        return $this->hasMany('Order');
    }

    /**
     * Password eloquent mutator
     *
     * @param string $value
     * @return void
     */
    public function setPasswordAttribute($value) 
    {
        $this->attributes['password'] = Hash::make($value); 
    }

    /**
     * Checks if activation key is valid
     *
     * @param string $key
     * @return boolean
     */
    public function isValidActivationKey($key)
    {
        return $this->activation_key === $key;
    }

    /** 
     * Checks if user is active
     *
     * @return boolean
     */
    public function isActive() 
    {
        return $this->active ? true : false;
    }

}

