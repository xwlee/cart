<?php

class OrderLines extends \Eloquent {

    protected $table = 'order_lines';

	protected $guarded = ['id'];

    protected $softDelete = true;

    public function order()
    {
        return $this->belongsTo('Order');
    }

    public function user()
    {
        return $this->belongsTo('Product');
    }

}
