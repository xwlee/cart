<?php

class Order extends \Eloquent {

	protected $guarded = ['id'];

    protected $softDelete = true;

    public function customer() 
    {
        return $this->belongsTo('Customer');
    }

    public function orderLines() 
    {
        return $this->hasMany('OrderLine');
    }

    public function products()
    {
        return $this->belongsToMany('Product', 'order_lines');
    }

}