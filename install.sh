#!/usr/bin/env bash

sudo apt-get update

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password secret'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password secret'

sudo apt-get install -y vim curl python-software-properties
sudo add-apt-repository -y ppa:ondrej/php5
sudo apt-get update

sudo apt-get install -y php5 apache2 libapache2-mod-php5 php5-curl php5-gd php5-mcrypt php5-readline mysql-server-5.5 php5-mysql git-core php5-xdebug

cat << EOF | sudo tee -a /etc/php5/mods-available/xdebug.ini
xdebug.scream=0
xdebug.cli_color=1
xdebug.show_local_vars=1
EOF

sudo a2enmod rewrite

sed -i "s/error_reporting = .*/error_reporting = E_ALL/" /etc/php5/apache2/php.ini
sed -i "s/display_errors = .*/display_errors = On/" /etc/php5/apache2/php.ini
sed -i "s/disable_functions = .*/disable_functions = /" /etc/php5/cli/php.ini

sed -i "s/AllowOverride None/AllowOverride All/" /etc/apache2/apache2.conf
sed -i 's/html//' /etc/apache2/sites-available/000-default.conf
rm -rf /var/www/html

VHOST=$(cat <<EOF
<VirtualHost *:80>
    ServerName cart.dev
    DocumentRoot /var/www/public
</VirtualHost>
EOF
)
echo "${VHOST}" > /etc/apache2/sites-enabled/000-default.conf

sudo service apache2 restart

curl -sS https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer

# Setup database
echo "CREATE DATABASE IF NOT EXISTS cart" | mysql -u root -psecret
echo "GRANT ALL PRIVILEGES ON cart.* TO 'vagrant'@'localhost' IDENTIFIED BY 'secret'" | mysql -u root -psecret

cd /var/www
php artisan migrate --seed --env=local
